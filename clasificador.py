#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Desarrolla un programa que permita ingresar una cadena
(un string) y evalúe si es un correo electrónico, un entero,
un real u otra cosa.
"""

import sys


def es_correo_electronico(string):
    if "@" in string:
        lista = string.split("@")
        if "." in lista[1]:
            return True
        else:
            return False
    else:
        return False

def es_entero(string):
    try:
        int(string)
        return True
    except ValueError:
        return False

def es_real(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

def evaluar_entrada(string):

    if string == "":
        print("No ha ingresado un ningún string")
        return None
    if es_correo_electronico(string):
        print("Es un correo electrónico.")
        return ""

    if es_entero(string):
        print("Es un entero.")
        return ""

    if es_real(string):
        print("Es un número real.")
        return ""

    print("No es ni un correo, ni un entero, ni un número real.")
    return ""



def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)

if __name__ == '__main__':
    main()
